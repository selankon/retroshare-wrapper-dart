/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */
import 'dart:io';

import 'package:retroshare_dart_wrapper/retroshare.dart';
import 'package:test/test.dart';
import 'package:retroshare_dart_wrapper/retroshare.dart' as rs;

class AUTH {
  static const String identityId = "bdbd397cf7800c5e085968e185633b50";
  static const String locationId = "814228577bc0c5da968c79272adcbfce";
  static const String passphrase = "test";
  static const String apiUser = "test";

  static initLocal() =>
      rs.initRetroshare(
          identityId: AUTH.identityId,
          locationId: AUTH.locationId,
          passphrase: AUTH.passphrase,
          apiUser: AUTH.apiUser
      );

  static initRemote() {
    setRetroshareServicePrefix("http://127.0.0.1:9091");
    rs.initRetroshare(
      identityId: "4b01ba1ab1ac85ef10cef012ea8b937e",
      locationId: AUTH.locationId,
      passphrase: "1234",
    );
  }
}

void main() {
  group('A group of tests', () {

    test("findAFile", () async {
      AUTH.initRemote();

      var filePath = "/storage/emulated/0/DCIM/Screenshots/Screenshot_2021-06-04-10-33-13-755_net.altermundi.elrepoio.jpg";

      var res = await rs.findAFileOnDiretoryTree(filePath);
      assert(res.name == filePath);
      print(res.name);
      print(res.hash);
    });

    test("reverseProxy", () async {

      // Key given by use "/rsremote/:userName"
      String apiUser = "userTest", key  = "FFb938Lykp";

      // This lines are a copy of rsApiCall on RetroShare.dart
      String baseUrl = "http://localhost:8888/rsremote";
      String path  = "/rsGxsForums/getForumsSummaries";
//    String path = "/rsJsonApi/getAuthorizedTokens";
      final basicAuth = rs.makeAuthHeader(apiUser, key);
      Map<String, dynamic> params = {
        'forumId': "479328f21c718e6307d5bfbe0d8ac8a0"
      };

      rs.setRetroshareServicePrefix(baseUrl);
      final res = await rs.rsApiCall(
        path,
//        params: params,
        basicAuth: basicAuth,
      );

      print(res);
    });

    test("updateIdentity", () async {
      AUTH.initLocal();

      var id = "175598f57711851b2258b0be8325c0a5";
      var name = "DartIdentity modified";
      var imgPath = "tmp/test.jpg";
      var image= RsGxsImage(await File(imgPath).readAsBytes());

      await RsIdentity.updateIdentity(
          id,
          name,
          image
      );
    });

  });

  group('RsGxsCircles', () {

    test("getCircleDetails", () async {
      AUTH.initLocal();
      // AUTH.initRemote();
      var list = await rs.RsGxsCircles.getCirclesSummaries();
      for (var circle in list){
        print("Found circle:");
        print(circle);
      }
      // String id = "8bfd87cabebd7e198f2d4cf4a3bd9aa3";
      // var detail = await rs.RsGxsCircles.getCircleDetails(id);
      // print(detail);
    });


  });

  group('RsFiles', () {
    test("ExtraFileRemove", () async {
      AUTH.initRemote();
      String hash = "ba38ba1602ac776a57a712043151283cd231971f";
      print(await rs.RsFiles.extraFileRemove(hash));
    });



    test("Get file info ", () async {
      AUTH.initRemote();
      String hash = "9a4198d5658374c2b3c242e2365319c07bfcbf0e";
      // String hash = "ba38ba1602ac776a57a712043151283cd231971f";
      for(int x = 0 ; x < 200 ; x++){
        print(x);
        var res = await rs.RsFiles.fileDetails(hash, x);
        if(res["fname"] != "") {
          print(res.toString());
          break;
        }
      }
    }, timeout: Timeout(Duration(minutes: 10)));

  });
}
